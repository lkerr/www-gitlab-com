<!-- PLEASE READ: See https://about.gitlab.com/handbook/marketing/blog/unfiltered for details on the Unfiltered blog process -->

<!-- All blog posts should have a corresponding issue. Create an issue now if you haven't already, and add the link in place of the placeholder link below -->
Closes https://gitlab.com/gitlab-com/www-gitlab-com/issues/your-issue-number-here

### Checklist for writer

- [ ] Link to issue added, and set to close when this MR is merged
- [ ] Add disclaimer copy to the top and bottom of your blog post file: https://about.gitlab.com/handbook/marketing/blog/unfiltered/#disclaimers
- [ ] Blog post file formatted correctly, including any accompanying images
- [ ] `unfiltered` category entered in frontmatter
- [ ] Review app checked for any formatting issues
- [ ] Reviewed by fellow team member
- [ ] Assign to someone on your team with maintainer access to merge

/label ~"unfiltered"
