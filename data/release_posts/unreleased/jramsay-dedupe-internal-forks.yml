features:
  secondary:
    - name: "Deduplicate forks of internal projects"
      available_in: [core, starter, premium, ultimate]
      documentation_link: "https://docs.gitlab.com/ee/administration/repository_storage_types.html#hashed-object-pools"
      reporter: jramsay
      stage: create
      issue_url: "https://gitlab.com/gitlab-org/gitlab/issues/33318"
      description: |
        Forking workflows makes it easy to contribute to any project by
        creating a copy of the upstream project to work on before opening a
        merge request to merge your changes into the upstream project. For
        popular projects, the server-side storage requirements needed for
        thousands of copies accumulate quickly and increase hosting costs.

        In GitLab 12.1, we introduced fork deduplication for public projects,
        but many organizations missed out on the benefits because they
        primarily use internal projects. In GitLab 12.5, creating a fork of
        public or internal projects creates an object pool (if one doesn’t
        already exist) and uses `objects/info/alternates` to reduce the storage
        requirements of forks.

        Thanks [Brian Kabiro](https://gitlab.com/briankabiro) for your
        [contribution](https://gitlab.com/gitlab-org/gitlab/merge_requests/19295)!
