---
layout: markdown_page
title: Product Vision - Conversion
---
 
### Overview:
The Growth Conversion Team at GitLab focuses on running experiments to increase the rate at which free accounts upgrade to become paying customers. Today, it can be challenging for a free user to understand the features that are available within each tier of GitLab when navigating within the app. We want to make it as easy as possible for teams to collaborate together on the features that are best for them and to explore purchasing on their own terms whether that’s starting a trial, speaking with someone at GitLab or upgrading on their own.
 
**Mission:**
* To continually improve the user experience when interacting with locked features, limits and trials.
 
**Team:**
 
Product Manager: [Sam Awezec](/company/team/#s_awezec) | 
Engineering Manager: [Jerome Ng](/company/team/#jeromezng) | 
UX Manager: [Jacki Bauer](/company/team/#jackib) | 
Product Designer: [Kevin Comoli](/company/team/#kcomoli) | 
Full Stack Engineer: [Alper Akgun](/company/team/#a_akgun) | 
Full Stack Engineer: [Vacancy](/company/team/#upsell-engineer-2)
 
**Conversion KPI:**
* Free to paid signup ARR growth rate
* Definition:  (Free to paid signup ARR in the current period - free to paid signup revenue in the prior period) / free to paid signup ARR in the prior period
* The Analysis can be found in the [Product KPIs Dashboard in Periscope](https://app.periscopedata.com/app/gitlab/527913/WIP:-Product-KPIs?widget=7065032&udv=0)
 
**Supporting performance indicators:**
 
* New ARPU
* IACV
* Free to paid conversion rate (free user upgrades / free user signups in period)
* Free trial conversion rate
* Product driven revenue by channel (e-commerce, sales rep assisted) & funnel
* E-commerce and sales rep assisted will track our core funnel efficiency. Funnel will allow us to understand the value of core actions taken within the app. We will define these actions to start - they may be ‘upgrade now’ ‘talk to sales’ ‘start a trial’ ‘request access’. We then take the total occurrences and multiply it by the close rate and ASP to understand the value of the action in-app.
**Other reports we’re going to be monitoring**
In-app “upgrade now” buttons
* In-app “upgrade now” button selection to customers app purchase and ASP
* In-app “upgrade now” button selection to sales representative purchase and ASP
Started trial after GitLab.com sign up
* Trial to customers app purchase and ASP
* Trial to sales representative purchase and ASP
 
GitLab.com influenced revenue
User
* Time from GitLab.com user sign up to .com purchase (customers app or through sales representative)
* Breakdown of percent of .com purchases from customers app vs sales representative
Group
* Time from GitLab.com group sign up to .com purchase  (customers app or through sales representative)
* Group ASP
* Breakdown of percent of .com purchases from customers app vs sales representative
 
GitLab self-hosted cross over revenue
User
* Time from GitLab.com user sign up to .com purchase (customers app or through sales representative)
* User ASP
* Breakdown of percent of .com purchases from customers app vs sales representative
Instance
* Time from GitLab.com group sign up to .com purchase  (customers app or through sales representative)
* Instance ASP
* Breakdown of percent of .com purchases from customers app vs sales representative
 
### Problems to solve
Have you experienced an issue working a free user attempting to upgrade or do you have ideas on how we can improve the experience? We’d love to hear from you!
  
### To effectively impact our KPI we'll focus on three core moments
  
Once a user has signed up for the free product, we have three core moments to convince them to become a customer. By getting them to initially activate and see value in the free product, exposing them to ah-ha moments with paid features, and ensuring they see value in the trial experience.
 
 1. **Initial product activation**
  
By focusing experiments on activation and increasing the activation rate, we will impact downstream efforts in points 2 and 3. Note, this still needs to be validated once we have user tracking in place. How we'll get there:
* Qualitative data collection - Understand what jobs GitLab is being hired to complete along with user-specific information so we can assist in making their adoption of GitLab as successful as possible.
* Quantitative experiments - We will use discoveries in the data collected in the bullet above to support onboarding experiments. For example, if we find common themes include an engineer signing up planning to migrate a single project over, then we bring them directly into the project migration process and walk them through it. If someone indicates they plan to bring multiple dev ops teams over, we can start by having them create a group and then projects, walking them through the process. 
     
 2. **Ah-ha moments with paid features and/or limits**
  
When users are actively using a tier of the product, they should be aware if GitLab has the capability to make their job easier. We can do this by displaying premium features at key moments in the use of the product. How we'll get there:
* Qualitative data collection - Survey existing free users to understand why they haven’t upgraded and ask what feature GitLab needs for them to upgrade. If users list features that we already have, then we know what features have discoverability issues.
* Quantitative experiments - We will run experiments where we increase the discoverability of paid features and the associated value. We will use both the qualitative and quantitative data collected over time to generate future experiment ideas.
 
3. **Trial and upgrade experience/value**
 
When an account starts a trial, they are making a conscious decision to test out paid features; this represents an opportune time for us to ensure we display the value of the paid features. We need to ensure that trial users experience the value of the paid features and decide to hire GitLab to improve their existing workflows. This same philosophy applies to users who are choosing to upgrade. How we'll get there:
* Qualitative data collection - Survey users as they start a trial asking what jobs they’re looking to complete within the trial.
* Quantitative experiments - We will focus experiments on how to ensure users are getting value out of trials as well as ensuring they are able to see the value in the paid tiers if they're debating upgrading. Our goal is to make it as easy as possible for the user to decide to hire GitLab. 
 
### Our approach
We will utilize the ICE framework (impact, confidence, ease) to ensure we’re best utilizing our time and resources. The following table displays the top issues we're currently prioritizing. 
 
| ICE Score | Description | Why/Hypothesis |
| ------ | ------ | ------ |
| 7 |  [Test new upgrade module for feature weights](https://gitlab.com/gitlab-org/growth/engineering/issues/49) |  We believe users may not actually see the value in some locked features, we want to setup a testing framework where we can easily test and iterate on these paywall states, this is the first test in this area. |
| 7 | [Expose Security nav item to a cohort of new signups](https://gitlab.com/gitlab-org/growth/engineering/issues/67) | We believe some users may not be aware that GitLab offers some of our premium features due to the navigation items being hidden to free users. This test will be the first test in a series where we expose the value of the premium features within the free navigation |
| 8.33 | [Expose an upgrade or trial option in the top right account dropdown](https://gitlab.com/gitlab-org/growth/product/issues/116/) | We believe that some users may not know where to go to start a trial or upgrade. This test will help us understand if discoverability is an issue for users when it comes to starting a trial or upgrading. |
| TBD | [Walk new signups through group/project creation process](https://gitlab.com/gitlab-org/growth/product/issues/122) | We believe that given how central a group/project is to the successful adoption of GitLab that we'd like to test walking new users through this process.  |
| TBD | [Onboard new users through an onboarding issue board](https://gitlab.com/gitlab-org/growth/product/issues/107) | We believe onboarding to a platform as large as GitLab is task that will likely take more than one session to complete and the order will be dependant on job GitLab has been hired to complete. This test will allow us to experiment with onboarding users to the issues feature while also giving them a home for their onboarding with issues already created for core actions they should be complete |
  
Our full backlog of work can be found [here](https://gitlab.com/groups/gitlab-org/growth/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aconversion). 
    
### Maturity
The team is brand new as of August  2019, our goal is to build out experimentation framework and the backlog of experiment ideas so we can become a well-oiled machine in the months to come.
 
### Helpful links
[GitLab Growth project](https://gitlab.com/gitlab-org/growth)
 
[KPIs & Performance Indicators](https://about.gitlab.com/handbook/product/metrics/)
 
Reports & Dashboards (these will be linked once they are created)
