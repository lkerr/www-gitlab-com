---
layout: markdown_page
title: "Category Vision - Interactive Application Security Testing (IAST)"
---

- TOC
{:toc}

## Description

<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas, 
and user journeys into this section. -->

Applications that are deployed to production are subject to attacks that exploit vulnerabilities. Many vulnerabilities can be identified and corrected prior to product deployment by running security scans on the source code ([SAST](https://docs.gitlab.com/ee/user/application_security/sast/)) or on exposed interfaces ([DAST](https://docs.gitlab.com/ee/user/application_security/dast/)).

Some vulnerabilities, however, cannot be identified by standalone static or dynamic application testing. This is where interactive security application testing comes in.

IAST works through software instrumentation, or the use of instruments to monitor an application as it runs and gather information about what it does and how it performs. IAST solutions instrument applications by deploying agents in running applications and continuously analyzing all application interactions initiated by manual tests, automated tests, or a combination of both to identify vulnerabilities in real time.[*](https://www.synopsys.com/software-integrity/resources/knowledge-database/what-is-iast.html) IAST can detect more vulnerabilities than SAST or DAST because the agent has visibility into:

* All the code for the application
* Runtime control and data flow information
* Configuration information
* HTTP requests and responses
* Libraries, frameworks, and other components
* Backend connection information
* Database connections

While IAST is similar to Runtime Application Self-Protection Security (RASP), the key difference is that IAST is focused on identifying vulnerabilities within the application and RASPs are focused protecting against cybersecurity attacks that may take advantages of those vulnerabilities or other attack vectors.

## Target audience and experience
<!-- An overview of the personas involved in this category. An overview 
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

## What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

## Maturity Plan
 - [Base Epic](https://gitlab.com/groups/gitlab-org/-/epics/344)
 
## Competitive landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

## Analyst landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->