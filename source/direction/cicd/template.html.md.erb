---
layout: markdown_page
title: "Product Section Vision - CI/CD"
---

{:.no_toc}

- TOC
{:toc}

## CI/CD Overview
<!-- Provide a general overview of the section, what is covered within it and introduce themes.
Include details on our current market share (if available), the total addressable market (TAM),
our competitive position, and high level feedback from customers on current features -->

The CI/CD section focuses on the following stages of the [DevOps Lifecycle](/stages-devops-lifecycle/):

- Code build/verification ([Verify](/direction/cicd#verify))
- Packaging/distribution ([Package](/direction/cicd#package))
- Software delivery ([Release](/direction/cicd#release))

You can see how the CI/CD stages relate to each other in the following infographic:

![Pipeline Infographic](/images/cicd/gitlab_cicd_workflow_verify_package_release_v12_3.png "Pipeline Infographic")

CI/CD is a large portion of the DevOps market with significant growth upside. The application release automation/CD and configuration management (mainly aligned to the Release stage) segment supported 984.7MM USD revenue in 2019, projected by 2022 to grow to 1617.0MM. The Continuous Integration, integrated quality/code review/static analysis/test automation, and environment management (mainly aligned to the Verify stage) represented an even larger 1785.0MM market in 2019, projected to grow to 2624.0MM. With a combined projected total of 4241.0MM by 2022, it's critical that we continue to lead here. __([market data, GitLab internal link](https://docs.google.com/spreadsheets/d/14j-C-9AzSRI2pEvX2zh42O34Y_EgDocEqffwv_KyJJ4/edit#gid=0))__

The CI/CD section has [41 members with 22 vacancies open](/company/team/?department=cicd-section). Tracking against plan can be seen on our [hiring chart](https://about.gitlab.com/handbook/hiring/charts/cicd-section/). Verify is at [year 7 maturity](/direction/maturity/#verify), Package is at [year 3 maturity](/direction/maturity/#package), and Release is at [year 4 maturity](/direction/maturity/#release).

### Monthly Q&A Video

We have a monthly internal Q&A related to the content on this page and the team's way of working. You can see the latest video below:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/lhep-b92tV0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Competitive Space and Positioning

CI/CD solutions have continued to rapidly innovate. Kubernetes itself, a massive driver of industry change, just turned five years old in 2019. Docker, now considered a mature and foundational technology, was only released in 2013. We do not expect this pace to slow down, so it's important we balance supporting the current technology winners (Kubernetes, AWS, Azure, and Google Cloud) with best-of-breed, built-in solutions, while at the same time avoiding over-investment on any single technology solution that could become legacy.

There are dangers to falling out of balance; Pivotal resisted embracing Kubernetes early and has [suffered for it](https://fortune.com/2019/07/29/ford-pivotal-write-down/). At the same time, technologies like Jenkins that deeply embraced what are now considered legacy paradigms are enjoying the long tail of relevancy, but are having to [scramble to remain relevant for the future](https://jenkins.io/blog/2018/08/31/shifting-gears/).

### Trend Towards Consolidation

Continuing apace in 2019 after Microsoft's 2018 [acquisition of GitHub](https://blogs.microsoft.com/blog/2018/10/26/microsoft-completes-github-acquisition/), consolidation as a trend seems here to stay. In January, [Travis CI was acquired by Idera](https://techcrunch.com/2019/01/23/idera-acquires-travis-ci/), and in February we saw [Shippable acquired by JFrog](https://techcrunch.com/2019/02/21/jfrog-acquires-shippable-adding-continuous-integration-and-delivery-to-its-devops-platform/). Atlassian and GitHub now both bundle CI/CD with SCM, alongside their ever-growing related suite of products.

It's natural for technology markets go through stages as they mature: when a young technology is first becoming popular, there is an explosion of tools to support it. New technologies have rough edges that make them difficult to use, and early tools tend to center around adoption of the new paradigm. Once the technology matures, consolidation is a natural part of the life cycle. GitLab is in a fantastic position to be ahead of the curve on consolidation, but it's a position we need to actively defend as competitors start to bring more legitimately integrated products to market.

### Making Continuous Delivery Real

Our product positioning for CI/CD is aligned to the one set forth in Jez Humble's classic [Continuous Delivery](https://continuousdelivery.com/) book. Reducing risk, getting features and fixes out quickly, and reducing costs/creating happier teams by removing the barriers to getting things done are principles that have stood the test of time. Modern CD pushes this even further by supporting more ways to microtarget users and incrementally make changes, and [Progressive Delivery](/direction/cicd/#progressive-delivery) has emerged as a set of best-practices that we're embracing in our product. Still, the [5 principles at the heart of continuous delivery](https://continuousdelivery.com/principles/) remain illustrative of the approaches we want to enable:

1. Build quality in
1. Work in small batches
1. Computers perform repetitive tasks, people solve problems
1. Relentlessly pursue continuous improvement
1. Everyone is responsible

One of the challenges with CD today is that it requires a suite of different tools to deliver: feature flag management, deployment automation, release orchestration, and more. GitLab's single application is uniquely positioned to provide a compelling solution here.

### Challenges
<!-- What are our constraints? (team size, product maturity, lack of brand, GTM challenges, etc). What are our market/competitive challenges? -->

- Highly competitive market with large competitors moving towards a single application, and smaller competitors innovating nimbly on features.
- GitHub in particular has a strong product positioning and adoption, and is our rival that we need to monitor most closely.
- Spinnaker (and to a certain extent Drone) are doing well around innovating on CD technology. Spinnaker in particular is enabling [collaborative CI/CD workflows using automation](https://opensource.com/article/19/8/why-spinnaker-matters-cicd) similar to what we want to provide.

### Opportunities

- Our single application is an incredible foundation on which to build. CI/CD in particular performs very well under analyst review and represents our "foot in the door" for many customers to the GitLab application at large.
- Continuous Delivery (Release stage) and Package stage features represent valuable new markets for us to enter and provide better, more integrated solutions.
- Established CI/CD solutions like TravisCI, Jenkins, and CircleCI are suffering from losing ground to consolidation as well as the more nimble innovators. We have an opportunity here to engage their customers with our better solution and bring them on board.

## Strategy
<!-- Where will the product be in 3 years? How will the customer's life/workflow be different in 3 years as a
result of our product? Also, should call out explicitly what's part of the three year plan but not being done
in the next year. -->

The strategy for CI/CD overall centers on the idea of enabling workflows that support teams interacting with the same concepts and tools around CI/CD; each may have their own views or dashboards in the product that support their day to day, but the information about the state of what is deployed where is available everywhere. For example, a person thinking about upcoming releases may interact mostly with an environment-based overview that helps them see upcoming changes as they flow through different ephemeral or permanent environments, but that state data exists everywhere it is relevant: 

- Testers looking at an individual issue can see which environment(s) that issue has been deployed to
- Developers reviewing a merge request have the Review App at their fingertips
- Feature flags link back to the issues and merge requests that introduced them for context
- Upcoming releases have burndown charts right in the overview
- Evidence collection for auditors happens automatically throughout the pipeline, with nothing special to set up

The end result of this kind of information flow is that even complex delivery flows become part of everyone's primary way of working. There isn't a context shift (or even worse, a switch into a different tool) needed to start thinking about delivery - that information is there, in context, from your first commit.

## Plan & Themes

Given our vision and strategy, our plan for the next year has been organized around a number of themes that support our direction:

### What we're doing

<%= partial("direction/cicd/themes/integrated_solutions") %>

<%= partial("direction/cicd/themes/multi_platform") %>

<%= partial("direction/cicd/themes/speedy_pipelines") %>

<%= partial("direction/cicd/themes/cool_things") %>

<%= partial("direction/cicd/themes/progressive_delivery") %>

<%= partial("direction/cicd/themes/mobile_support") %>

<%= partial("direction/cicd/themes/compliance_as_code") %>

### What we're not doing

<%= partial("direction/cicd/themes/pull_pipelines") %>

## Stages & Categories

The CI/CD section is composed of three stages, each of which contains several categories. Each stage has an overall strategy statement below, aligned to the themes for CI/CD, and each category within each stage has a dedicated vision page (plus optional documentation, marketing pages, and other materials linked below.)

<%= partial("direction/cicd/strategies/verify") %>

<%= partial("direction/cicd/strategies/package") %>

<%= partial("direction/cicd/strategies/release") %>

## What's Next
<!-- Conclude with What must we get done in the next 12 months? What won't be done? Then
transition into direction items highlighting those themes across the relevant stages -->

It's important to call out that the below plan can change any moment and should not be taken as a hard commitment, though we do try to keep things generally stable. In general, we follow the same [prioritization guidelines](/handbook/product/#prioritization) as the product team at large. Issues will tend to flow from having no milestone, to being added to the backlog, to being added to this page and/or a specific milestone for delivery.

<%= direction["all"]["all"] %>
