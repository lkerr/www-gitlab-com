---
layout: handbook-page-toc
title: "Acquisition Performance Indicators"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Acquisition velocity

Acquisition velocity is the pace at which GitLab acquires companies. The target is 10 per year.

## Acquisition success

Acquisition success measures how successful GitLab at integrating the new teams and technologies acquired. An acquisition is a success if it ships 70% of the acquired company's product functionality as part of Gitlab within three months after acquisition. The acquisition success rate is the percentage of acquired companies that were successful. The target is 70% success rate.

## Qualified acquisition targets

An acquisition target is considered 'Qualified' if it:
1. Fits the [acquisition target profile](/handbook/acquisitions/#acquisition-target-profile)
1. Has product functionality which is either:
   1. A new category on our roadmap or
   1. At a more advanced maturity than our current categories

Target is 1000 qualified acquisition targets per year.
