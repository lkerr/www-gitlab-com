To assign weights to issues in a future milestone, we ask two team members to
take the lead each month. They can still ask questions - of each other, of the
rest of the team, of the stable counterparts, or anyone else - but they are the
initial. This is currently shared across the three Plan backend teams: [Project
Management], [Portfolio Management], and [Certify], so people will be asked to
weight some issues outside of their immediate team.

We will [assess whether or not we want to scope this down to individual
teams in January CY20][follow-up].

[Project Management]: /handbook/engineering/development/dev/plan-project-management-be/
[Portfolio Management]: /handbook/engineering/development/dev/plan-portfolio-management-be/
[Certify]: /handbook/engineering/development/dev/plan-certify-be/
[follow-up]: https://gitlab.com/gitlab-org/plan/issues/22

To weight issues, they should:

1. Look through the issues on the upcoming milestone and those in 
   ~workflow::"planning breakdown", filtered by Weight:None.
2. For those they understand, they add a weight. If possible, they also add a
   short comment explaining why they added that weight, what parts of the code
   they think this would involve, and any risks or edge cases we'll need to
   consider.
3. Timebox the issue weighting overall, and for each issue. The process is
   intended to be lightweight. If something isn't clear what weight it is, they
   should ask for clarification on the scope of the issue.
4. If two people disagree on the weight of an issue, even after explaining their
   perceptions of the scope, we use the higher weight.
5. Start adding weights around a week before the weights for a milestone
   are due. Finishing earlier is better than finishing later.

The rotation for upcoming releases is:

| Release | Weights due | Engineer       | Engineer          |
| ---     | ---         | ---            | ---               |
| 12.3    | 2019-08-07  | Felipe Cardozo | Heinrich Lee Yu   |
| 12.4    | 2019-09-07  | Charlie Ablett | Mario de la Ossa  |
| 12.5    | 2019-10-13  | Brett Walker   | Alexandru Croitor |
| 12.6    | 2019-11-13  | Jarka Košanová | Patrick Derichs   |
| 12.7    | 2019-12-13  | Jan Provaznik  | Eugenia Grieff    |
| 12.8    | 2020-01-13  | Felipe Cardozo | Mario de la Ossa  |
