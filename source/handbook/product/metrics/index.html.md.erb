---
layout: handbook-page-toc
title: "Product Performance Indicators" 
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Key Performance Indicators

Please note: Product KPIs are mapped 1 to 1 with our Growth teams in order to focus those teams on experiments to improve our KPIs. Additional performance indicators will be tracked and may add value, but should ultimately drive one or more KPI.

### Average Stages per User
Stage Monthly Active User (SMAU) divided by [MAU](/handbook/product/metrics/#monthly-active-user-mau). Average Stages per User is meant to capture the number of DevOps stages the average user is using on a monthly basis. We hope to add this metric to the [stage maturity page](/direction/maturity/), alongside number of contributions. 

### Category Maturity Achievement
Percentage of category maturity plan achieved per quarter

### Paid Net Promoter Score
Abbreviated as the PNPS acronym, please don't refer to it as NPS to prevent confusion. Measured as the percentage of paid customer "promoters" minus the percentage of paid customer "detractors" from a [Net Promoter Score](https://en.wikipedia.org/wiki/Net_Promoter#targetText=Net%20Promoter%20or%20Net%20Promoter,be%20correlated%20with%20revenue%20growth.) survey.  Note that while other teams at GitLab use a [satisfaction score](/handbook/business-ops/data-team/metrics/#satisfaction), we have chosen to use Net Promoter Score in this case so it is easier to benchmark versus other like companies.  Also note that the score will likely reflect customer satisfaction beyond the product itself, as customers will grade us on the total customer experience, including support, documentation, billing, etc.

## Other Shared Performance Indicators for GitLab.com and Self-Managed

### Active User
An active user is any GitLab user who has performed at least 1 [Event](https://docs.gitlab.com/ee/api/events.html) within the previous 28 days.  

### Monthly Active Users (MAU)
The MAU for a given month is the number of Active Users (with the trailing 28-day window) on the *last* day of the month.
For example, the MAU for January would include users who performed events during the window between January 3 and January 31. The MAU window for February 2019 would be from February 1 - 28. 

[Dashboard - GitLab.com Health](https://app.periscopedata.com/app/gitlab/461561/GitLab.com-Health)

### Acquisition (New User)
Amount of new users who signed up for a GitLab account (GitLab.com or Self-Managed) in a given month.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1422)

### Expansion
Amount of paid groups that added users to the namespace in a given month.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1430)

### CI Pipeline Minute Consumption
Total number of CI Runner Minutes consumed in a given month.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1431)

### User Return Rate
Percent of users or groups that are still active between the current month and the prior month.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1433)

### Churn
The opposite of User Return Rate. The percentage of users or groups that are no longer active in the current month, but were active in the prior month.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1466)

### Projects
Number of existing [Projects](https://docs.gitlab.com/ee/user/project/) at a specified point in time. This number currently includes [Archived Projects](https://docs.gitlab.com/ee/user/project/settings/#archiving-a-project).

[Dashboard](https://app.periscopedata.com/app/gitlab/461561/GitLab.com-Health)

### New Projects
Number of new [Projects](/handbook/product/metrics/#projects) created in a calendar month.

[Dashboard](https://app.periscopedata.com/app/gitlab/461561/GitLab.com-Health)

### New Merge Requests
Number of [Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/) created in a calendar month.

[Dashboard](https://app.periscopedata.com/app/gitlab/461561/GitLab.com-Health)

### New Issues
Number of [Issues](https://docs.gitlab.com/ee/user/project/issues/) created in a calendar month.

[Dashboard](https://app.periscopedata.com/app/gitlab/461561/GitLab.com-Health)

### Provably successful Direction items
This metric reports on the percentage of Direction items that have met or
exceeded their respective success performance indicators. For each feature labeled ~Direction,
there should be a defined success metric, and telemetry configured to report
on that success metric to determine if it was provably successful.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1467)

### Community Engagement On GitLab Issues
Percent of open GitLab issues that have comments from customers and wider
community members. This dashboard also measures relative engagement over time.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/2863)

## Other GitLab.com Only Performance Indicators

### Conversion
Amount of users who moved from a free tier to a paid tier in a given month.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1428)

### Active Churned User
A GitLab.com user, who is not a MAU in month T, but was a MAU in month T-1.

[Dashboard](https://app.periscopedata.com/app/gitlab/461561/GitLab.com-Health)

### Active Retained User
A GitLab.com user, who is a MAU both in months T and T-1.

[Dashboard](https://app.periscopedata.com/app/gitlab/461561/GitLab.com-Health)

### New User
A newly registered GitLab.com user - no requirements on activity.

[Dashboard](https://app.periscopedata.com/app/gitlab/461561/GitLab.com-Health)

### Churned Returning User
A GitLab.com user, who is not a new user and who was not a MAU in month T-1, but is a MAU in month T.

[Dashboard](https://app.periscopedata.com/app/gitlab/461561/GitLab.com-Health)

### Paid User
A GitLab.com [Licensed User](/handbook/finance/operating-metrics/#licensed-users)

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1445)

### Paid MAU
A [paid](/handbook/finance/gitlabcom-metrics/index.html#paid-user) [MAU](/handbook/finance/gitlabcom-metrics/index.html#monthly-active-user-mau).

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1446)

### Active Retained Paid User
A [paid](/handbook/finance/gitlabcom-metrics/index.html#paid-user) [Active Retained User](/handbook/finance/gitlabcom-metrics/index.html#active-retained-user)

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1447)

### Monthly Active Group (MAG)
A GitLab [Group](https://docs.gitlab.com/ee/user/group/), which contains at least 1 [project](https://docs.gitlab.com/ee/user/project/) since inception and has at least 1 [Event](https://docs.gitlab.com/ee/api/events.html) in a calendar month.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1448)

### Active Churned Group
A GitLab.com group, which is not a MAG in month T, but was a MAG in month T-1.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1449)

### Active Retained Group
A GitLab.com group, which is a MAG both in months T and T-1.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1450)

### New Group
A newly created top-level GitLab.com group - no requirements on activity.

[Dashboard](https://app.periscopedata.com/app/gitlab/461561/GitLab.com-Health)

### Paid Group
A GitLab.com group, which is part of a paid plan, i.e. Bronze, Silver or Gold. [Free licenses for Ultimate and Gold](/blog/2018/06/05/gitlab-ultimate-and-gold-free-for-education-and-open-source/) are currently included.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1452)

### Paid MAG
A [paid](/handbook/finance/gitlabcom-metrics/index.html#paid-group) [MAG](/handbook/finance/gitlabcom-metrics/index.html#monthly-active-group-mag).

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1453)

### Active Retained Paid Group
A [paid](/handbook/finance/gitlabcom-metrics/index.html#paid-group) [Active Retained Group](/handbook/finance/gitlabcom-metrics/index.html#active-retained-group)

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1454)

### Paid Group Member
A GitLab.com user, who is a member of a [Paid Group](/handbook/product/metrics/#paid-group).

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1455)

### Paid CI pipeline minute consumption rate
The percent of users or groups that pay for additional CI pipeline minutes.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1432)

## Other Self-Managed Only Performance Indicators

### Active Hosts
The count of active [Self Hosts](/pricing/#self-managed), Core and Paid, plus SaaS

### Lost instances
A lost instance of self-managed GitLab didn't send a usage ping in the given month but it was active in the previous month

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1461)
