---
layout: markdown_page
title: "Authorization Matrix"
---
For instruction on how to get approval to purchase goods or services see our [Procure to Pay Process](/handbook/finance/procure-to-pay/).

## Signing Legal Documents

The table below designates team members that are authorized to sign legal documents, with the exception of NDAs covering a physical visit of another organization.
When working with legal agreements with vendors, consultants, and so forth, bear in mind the [signature authorization matrix](/handbook/finance/authorization-matrix/).
If you need to obtain approval for a vendor contract, please create a confidential issue in the finance issue tracker using our [Contract Approval Workflow](/handbook/finance/procure-to-pay/).

For all other documents that need to be signed, filled out, sent, or retrieved electronically, please do your best to fill out the form using the handbook and [wiki](https://gitlab.com/gitlab-com/finance/wikis/company-information ) then e-mail it to `legal@` with the following information:

1. Names and email addresses of those who need to sign the document.
1. Any contractual information that needs to be included in the document.
1. Deadline (or preferred timeline) by which you need the document prepared (i.e. staged in [HelloSign](https://www.hellosign.com) for relevant signatures)
1. Include a link to the relevant issue in the body of the hellosign email message.
1. If the vendor insists on sending the document via electronic issue please provide them a link to include in the request.
1. Names and email addresses of those who need to be cc-ed on the signed document.

The process that Legal will follow is:

1. Review the document and prepare as requested.
1. Have the requestor check the prepared document, AND obtain approval from the CFO or CEO (such approval may be explicit in the email thread that was sent to `legal@`, in which case a second approval is not needed unless there have been significant edits to the document).
1. Requestor shall stage the document for signing in HelloSign and cc (at minimum) `legal@`.
1. Once signed: 
a. For customer contracts, the Requestor needs to attach the document to the applicable Contracts Object in Salesforce, and fill out all applicable fields in the Contracts Object. 
b. For vendor agreements, Requestor will [file the document in ContractWorks](/handbook/legal/vendor-contract-filing-process/).

## Authorization Matrix

<table border="1">
  <tr>
    <td></td>
    <td colspan="4" align="center"><b>Functional Approval<sup>(4)</sup></b></td>
    <td colspan="3" align="center"><b>Financial Approval</b></td>
  </tr>
  <tr>
    <td></td>
    <td><b>Team Member</b></td>
    <td><b>Exec Team</b></td>
    <td><b>CEO</b></td>
    <td><b>Board</b></td>
    <td><b>PAO or VP, FP&A</b></td>
    <td><b>CFO</b></td>
    <td><b>CLO</b></td>
  </tr>
  <tr>
    <td colspan="8" align="center"><b>Operating Expenses</b></td>
  </tr>
  <tr>
    <td>Up to $25K</td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Approves</sup></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>&gt;$25K up to $100K</td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>&gt;$100K up to $250K</td>
    <td></td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td>Approves</td>
    <td></td>
  </tr>
  <tr>
    <td>above $250K</td>
    <td></td>
    <td></td>
    <td>Approves <sup>(1)</sup></td>
    <td>Approves<sup>(2)</sup></td>
    <td></td>
    <td>Approves</td>
    <td></td>
  </tr>
  <tr>
    <td colspan="8" align="center"><b>Capital Asset Additions</b></td>
  </tr>
  <tr>
    <td>Up to $5K</td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td></td>
    <td>N/A</sup></td>
    <td>N/A</sup></td>
    <td></td>
  </tr>
  <tr>
    <td>&gt;$5K up to $10K</td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>&gt;$10K up to $100K</td>
    <td></td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>above $100K</td>
    <td></td>
    <td></td>
    <td>Approves <sup>(1)</sup></td>
    <td>Approves<sup>(2)</sup></td>
    <td></td>
    <td>Approves</td>
    <td></td>
  </tr>
  <tr>
    <td colspan="8" align="center"><b>Legal Contracts</b></td>
  </tr>
  <tr>
    <td>Standard Terms</td>
    <td></td>
    <td>Signs</td>
    <td></td>
    <td></td>
    <td>Signs</td>
    <td></td>
    <td>Signs/Approves</td>
  </tr>
  <tr>
    <td>Changes to Standard Terms and Vendor Contracts</td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td>Signs</td>
    <td></td>
    <td>Signs/Approves</td>
  </tr>
  <tr>
    <td>Non standard terms > $0.5M annual revenue</td>
    <td></td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td>Signs</td>
    <td>Signs/Approves</td>
  </tr>
  <tr>
    <td>NDA - GitLab Template</td>
    <td></td>
    <td>Signs</td>
    <td></td>
    <td></td>
    <td>Signs</td>
    <td></td>
    <td>Signs/Approves</td>
  </tr>
  <tr>
    <td>NDA - Third Party</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>Signs</td>
    <td></td>
    <td>Signs/Approves</td>
  </tr>
  <tr>
    <td colspan="8" align="center"><b>Bad debt write-off</b></td>
  </tr>
  <tr>
    <td>Up to $10K</td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>$10K to $100K</td>
    <td></td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>$100K plus</td>
    <td></td>
    <td></td>
    <td>Approves</td>
    <td>Advised</td>
    <td></td>
    <td>Approves</td>
    <td></td>
  </tr>
  <tr>
    <td colspan="8" align="center"><b>Compensation/Hiring - non-executive:</b></td>
  </tr>
  <tr>
    <td>Initial hiring Budgeted</td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Initial hiring Non-Budgeted</td>
    <td></td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Use of Search Firm</td>
    <td></td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Increases (Budgeted)</td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Increases (Not Budgeted)</td>
    <td></td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td>Approves</td>
    <td></td>
  </tr>
  <tr>
    <td>Bonuses</td>
    <td></td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td>Approves</td>
    <td></td>
  </tr>
  <tr>
    <td>Commission scales</td>
    <td></td>
    <td></td>
    <td>Approves</td>
    <td>Approves</td>
    <td></td>
    <td>Approves</td>
    <td></td>
  </tr>
  <tr>
    <td>Options</td>
    <td></td>
    <td></td>
    <td>Recommends</td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Executive Compensation - All</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Approves<sup>(3)</sup></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="8" align="center"><b>Benefit changes</b></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td>Approves</td>
    <td></td>
  </tr>
  <tr>
    <td colspan="8" align="center"><b>Employee Travel &amp; Entertainment</b></td>
  </tr>
  <tr>
    <td>Non Billable Expenses:</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Up to $5K</td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>&gt;$5K</td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>&gt;$50K</td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td></td>
    <td>Approves</td>
    <td></td>
  </tr>
  <tr>
    <td colspan="8" align="center"><b>Transfer of funds among GitLab entities</b></td>
  </tr>
  <tr>
    <td>Up to $500K</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>Approves</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Over $500K</td>
    <td></td>
    <td></td>
    <td>Approves(5)</td>
    <td></td>
    <td></td>
    <td>Approves</td>
    <td></td>
  </tr>
  <tr>
    <td colspan="8" align="center"><b>Treasury Management</b></td>
  </tr>
  <tr>
    <td colspan="4">Letters of Credit</td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="4">Guarantees</td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="4">Debt/Loan Financings</td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="4">Loan Renewals</td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="4">Investment Policy</td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="4">Equity Financings</td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="4">Opening Closing Bank Accounts</td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="4">Establish, liquidate, or change the legal status of an entity</td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="4">Acquire an entity</td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="4">Decisions to file a lawsuit or accept an injunction or consent decree (other than collection of receivables in due course)</td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="4">Decision to withdraw from/settle a lawsuit > $50,000</td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="4">Hiring or terminating corporate counsel</td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="4">Amendment to subsidiary charter or by-laws</td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="4">Change to equity structure and option plans</td>
    <td>Approves</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>

**Notes/Comments**:

- (1) If in Plan
- (2) If Not Included in Company Plan
- (3) Approved by Compensation Committee (or Board if no Compensation Committee)
- (4) Approval from [function](/company/team/structure/#table) responsible for supporting associated objectives and goals 
- (5) CEO review of transaction should ensure request submitted from member of accounting team and in-line with past practice.
- (6) Recurring consumption operating costs on approved contracts do not require multiple approvals. Spend analysis is monitored on a quarterly basis against Plan. Approved costs in this category include hosting services for gitlab.com, bounty programs, lead generation ad placement spend.
- (7) Only applies to approvals outside of the discount authorization matrix.

## Banking Controls

- All accounts are to be established so initiator and approver must be different.
