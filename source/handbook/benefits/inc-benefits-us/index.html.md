---
layout: handbook-page-toc
title: "GitLab Inc (US) Benefits"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

## Specific to US based team members
{: #us-specific-benefits}

US based benefits are arranged through [Lumity](https://lumity.com/). The benefits decision discussions are held by People Operations, the CFO, and the CEO to elect the next year's benefits by the carrier's deadlines. People Operations will notify the team of open enrollment as soon as details become available.  

Please review the full [summary plan description](https://drive.google.com/file/d/1K8uybZ_pQc-XxpOaIEqnSN-UlvZCsf1W/view?usp=sharing) of all related health benefits.   

If you have any questions regarding benefits please reach out to People Operations directly. If you have any questions with the Lumity platform, feel free to reach out to their support personnel at `support@lumity.com`.

Carrier ID cards are normally received within weeks of submitting your benefit elections. If you or your medical providers are in need of immediate confirmation of your coverage, please contact the carrier directly.

If you have existing coverage when joining GitLab (e.g. coverage for an additional month from your prior employer), you have the option of enrolling in GitLab's coverage after your prior coverage terminates. If you wish to do this, you should register with Lumity during onboarding and waive all coverages. Once your previous coverage is terminated, you should sign up for coverage through Lumity on or within 30 days of the termination date by initiating a Qualifying Life Event and providing proof of coverage termination.

GitLab covers **100% of team member contributions and 66% for spouse, dependents, and/or domestic partner** of premiums for medical, dental, and vision coverage. Plan rates are locked through December 31, 2019.

More information on the processed deductions in payroll from Lumity can be found under [Lumity Payroll Processes for GitLab Inc.](/handbook/finance/accounting/#lumity-payroll-processes-for-gitlab-inc) on the [Accounting and Reporting page](/handbook/finance/accounting/).

## 2020 Plan Year Open Enrollment

The Compensation and Benefits team at GitLab is currently undergoing a renewal of all health plans for US based team members.

* Schedule of Renewal Process
    * August 2019: Strategic Planning with Lumity/Lumity to receive proposals from Vendors
    * September 2019: Renewal Meeting to review quotes with Lumity
    * October 2019: Benefit/Plan Finalization
    * November 2019: Open Enrollment will open to GitLab team members in the US, 2019-11-11 to 2019-11-22
    * December 2019: ID Cards mailed
    * January 2020: Plan starts

* Summary of Changes:
  * 2020 UHC Plans
  * 2020 Kaiser Plans
  * There are no changes to Dental and Vision through Cigna (in plan or cost)
  * Disability and Life (Voluntary and Basic) will move from Cigna to UHC
  * Please note that you must actively elect an FSA plan for 2020 if you would like to participate.
  * This is a passive open enrollment, meaning if you do not elect any changes your plans from 2019 will carry into 2020.

* Open Enrollment Processes through Lumity: [Enrollment Instructions](https://drive.google.com/file/d/18CxmikkSvyrBtpfWmD3n_ofcv09kt-Dy/view?usp=sharing) and [Open Enrollment Benefits Guide](https://drive.google.com/file/d/16HO4PfHzdWQA0sqnYfVyEhquJrso8IUW/view?usp=sharing)

## Group Medical Coverage

GitLab offers plans from United Health Care for all states within the US as well as additional Kaiser options for residents of California, Hawaii, and Colorado. Deductibles for a plan year of 2019-01-01 to 2019-12-31.  

For additional information on how [Benefits](https://drive.google.com/file/d/0B4eFM43gu7VPVS1aOFdRdXRSVzZ5Z3B0dEVKUXJIWm1zZXRj/view?usp=sharing) or [HSAs](https://drive.google.com/file/d/0B4eFM43gu7VPbUo1VFNFVFlQNlUyV0xQZkE0YXQyZENSNU1j/view?usp=sharing) operate, please check out the documentation on the Google Drive or Lumity's [resources](https://employee-resources.lumity.com/help).

_If you already have current group medical coverage, you may choose to waive or opt out of group health benefits. If you choose to waive health coverage, you will receive a $300.00 monthly benefit allowance and will still be able to enroll in dental, vision, optional plans, and flexible spending accounts._

If you do not enroll in a plan within your benefits election period, you will automatically receive the medical waiver allowance.

GitLab has confirmed that our medical plans are CREDITABLE. Please see the attached [notice](https://docs.google.com/document/d/1qFK-MnErpOSTXli4G_c0ze-UeAU4szAx-GMoNnizQ_4/edit?usp=sharing). If you or your dependents are Medicare eligible or are approaching Medicare eligibility, you will need this notice to confirm your status when enrolling for Medicare Part D.

### Eligibility

Any active, regular, full-time team member working a minimum of 30 hours per week are eligible for all benefits. Benefits are effective on your date of hire. Others eligible for benefits include:
  * Your legal spouse or domestic partner,
  * Your dependent children up until age 26 (including legally adopted and stepchildren), and/or
  * Any dependent child who reaches the limiting age and is incapable of self-support because of a mental or physical disability

Note: If you and an eligible dependent (as defined above) are both employed by GitLab, you may only be covered by GitLab’s coverage once. This also applies to enrolling in either your own supplemental life insurance or supplemental spouse/dependent life insurance through your dependent who is employed by GitLab, but not both.

### United Health Care Medical Plans

To determine your specific plan name, log in to the Lumity system and click "My Benefits" - View Button > Plan Name (e.g. GitLab UHC HSA 2000 + Infertility). The benefits summary that comes up has the network name IN the plan name (for example: Select Plus PPO HDHP / California  - Select Plus / HSA - Plan AXEX).

When reviewing the UHC [Find a Doctor site](https://www.uhc.com/find-a-physician), you will get to the Network selection page and select the appropriate network - Select Plus or Select, based on plan enrollment.

#### UHC 2019 Calendar Year Plans

**Coverages:**

| Plan Details               | [UHC - HSA](https://drive.google.com/file/d/1rJxjFZ2P3_TgK6Uc5_kZsDmnkF0x5Dmh/view?usp=sharing)              | [UHC - EPO**](https://drive.google.com/file/d/1Qab_xuMkNZ8UwcnmSgCiYpzVuJONT6WE/view?usp=sharing)       | [UHC - PPO](https://drive.google.com/file/d/1mfLGdksp7EptbZp1Fi1pZ4fNY1jcJfeR/view?usp=sharing)         |
|:---------------------------|:----------------------:|:---------------:|:-----------------:|
| Deductible (Single/Family) | $2,000 / $2,700        |   $0 / $0       |  $500 / $1,000    |
| OOP Max (Single/Family)    | $4,000 / $4,000        | $2,500 / $5,000 | $3,000 / $6,000   |
| PCP/Specialist Copay       | 20% / 20%              | $20 / $20       | $20 / $20         |
| Coinsurance                | 20%                    | 0%              | 10%               |
| Emergency Room             | 20%                    | $100            | $100              |
| Urgent Care                | 20%                    | $50             | $50               |
| Hospital Inpatient         | 20%                    | $250            | 10%               |
| Hospital Outpatient        | 20%                    | 0%              | 10%               |
| Rx - Deductible            | -                      | -               | -                 |
| Generic                    | $10                    | $10             | $10               |
| Brand - Preferred          | $30                    | $30             | $30               |
| Brand - Non-Preferred      | $50                    | $50             | $50               |
| Specialty Drugs            | $50                    | 20% up to $250  | 20% up to $250    |
| Plan Specifics             | AXEX (Select Plus HSA) | PVK (Select)    | PR1 (Select Plus) |
| Rx Plan                    | [C2-HSA](https://www.uhc.com/content/dam/uhcdotcom/en/Pharmacy/PDFs/Traditional-3-Tier-PDL-Eff-July-1-2019.pdf)                 | [Rx Plan: 464](https://www.uhc.com/content/dam/uhcdotcom/en/Pharmacy/PDFs/Traditional-4-Tier-PDL-Eff-July-1-2019.pdf)    | [Rx Plan: 464](https://www.uhc.com/content/dam/uhcdotcom/en/Pharmacy/PDFs/Traditional-4-Tier-PDL-Eff-July-1-2019.pdf)      |

**At this time, coverage by the EPO is not available in the following states: AL, AR, AZ, LA, MT, NC, NM, OK**

Team members impacted by the EPO restrictions were sent an email from Lumity support on 2019/08/29 with the option to choose either the PPO or HSA plan. These changes will go into effect on 2019/10/01 with any contributions towards deductibles and out-of-pocket maximums being carried over into the new plan. For the team members that were sent a direct communication that they are impacted, they will have until 2019/09/20 to choose an alternate plan or be automatically enrolled in the PPO. New hires located in an affected state are being restricted from enrolling into the EPO plan during their new hire enrollment. Any questions can be directed to the People Operations Analyst.

**Team Member Costs:**

The following costs are monthly rates which would be deducted from your paycheck.

| Tier                  | HSA | EPO  | PPO  |
|-----------------------|:---:|:----:|:----:|
| Team Member Only         | $0  | $0   | $0   |
| Team Member + Spouse     | $0  | $168 | $168 |
| Team Member + Child(ren) | $0  | $144 | $144 |              
| Family                | $0  | $312 | $312 |

Note: For the **team member only HSA**, GitLab will contribute $100 per month. For residents of California, Alabama, and New Jersey this additional contribution is taxable on the state level. If you are 55 or older and would like to contribute an additional amount for the HSA catch up, please reach out to support@lumity.com and People Ops at GitLab.

Domestic Partner Reimbursements: If the team member is not legally married to their domestic partner, the domestic partner’s expenses are not eligible for disbursement from the HSA. However, if the domestic partner is covered under the family HDHP through the team member, the domestic partner can open their own HSA and contribute up to the full family contribution maximum. The team member may also contribute up to the full family contribution maximum to their own HSA.

#### UHC 2020 Calendar Year Plans

**Coverages:**

In Network:

| Plan Details               | UHC - HSA              | UHC - EPO**        | UHC - PPO         |
|:---------------------------|:----------------------:|:------------------:|:-----------------:|
| Deductible (Single/Family) | $2,000 / $2,800        | $0 / $0            | $500 / $1,000     |
| OOP Max (Single/Family)    | $4,000 / $4,000        | $2,500 / $5,000    | $3,000 / $6,000   |
| Primary Care Visit         | 20%                    | $20 per visit      | $20 per visit     |
| Specialist Visit           | 20%                    | $20 per visit      | $20 per visit     |
| Urgent Care                | 20%                    | $50 per visit      | $50 per visit     |
| Emergency Room             | 20%                    | $100 per visit     | $100 per visit    |
| Hospital Inpatient         | 20%                    | $250 per admission | 10%               |
| Hospital Outpatient        | 20%                    | 0%                 | 10%               |
| Generic                    | $10                    | $10                | $10               |
| Brand - Preferred          | $30                    | $30                | $30               |
| Brand - Non-Preferred      | $50                    | $50                | $50               |
| Specialty Drugs            | $50                    | 20% up to $250     | 20% up to $250    |

** At this time, coverage by the EPO is not available in all states. Where an EPO is not available, an additional PPO plan that is an in-network copayment is available for selection.


**Team Member Costs:**

The following costs are monthly rates which would be deducted from your paycheck.

| Tier                     | HSA | EPO  | PPO  |
|--------------------------|:---:|:----:|:----:|
| Team Member Only         | $0  | $0   | $0   |
| Team Member + Spouse     | $0  | $214 | $204 |
| Team Member + Child(ren) | $0  | $164 | $156 |              
| Family                   | $0  | $365 | $348 |

### Kaiser Medical Plans

#### Kaiser 2019 Calendar Year Plans  

**Coverages:**

| Plan Details               | [HMO 20 NorCal](https://drive.google.com/file/d/1EKsRhvN5QsrBEenNXl1_lPFar8StqWhQ/view?usp=sharing)        | [HMO 20 SoCal](https://drive.google.com/file/d/1v22n5cJFLI052NkATSbMZ2NRHB-n0NkU/view?usp=sharing)    | [HMO 20 CO](https://drive.google.com/file/d/1GaGjXUHkQmdXmXi02N8e2DVrYiAcT6VA/view?usp=sharing)       | [HMO 20 HI](https://drive.google.com/file/d/1R6kVl8h3YP-qcnxopNoi9LEZTJziLB2M/view?usp=sharing)       |
|----------------------------|:--------------------:|:---------------:|:---------------:|:---------------:|
| Deductible (Single/Family) | $0                   | $0              | $0              | $0              |
| OOP Max (Single/Family)    | $1,500 / $3,000      | $1,500 / $3,000 | $2,000 / $4,000 | $2,500 / $7,500 |
| PCP/Specialist Copay       | $20 / $35            | $20 / $35       | $20 / $35       | $20 / $20       |
| Coinsurance                |                      |                 |                 | 10%             |
| Emergency Room             | $100                 | $100            | $100            | $100            |
| Urgent Care                |                      |                 |                 |                 |
| Hospital Inpatient         | $250/admit           | $250/visit      | $300/admit      | 10%             |
| Hospital Outpatient        | $35/procedure        | $35/procedure   | $100            | 10%             |
| **Rx - Deductible**        |                      |                 |                 |                 |
| Generic                    | $10                  | $10             | $10             | $3 / $15        |
| Brand - Preferred          | $35                  | $35             | $30             | $50             |
| Brand - Non-Preferred      | $35                  | $35             | $50             | $50             |

**Team Member Costs:**

The following costs are monthly rates which would be deducted from your paycheck.

| Tier                  | HMO CA North | HMO CA South | HMO CO | HMO HI |
|-----------------------|:------------:|:------------:|:------:|:------:|
| Team Member Only         | $0           | $0           | $0     | $0     |
| Team Member + Spouse     | $240         | $216         | $240   | $168   |
| Team Member + Child(ren) | $192         | $168         | $216   | $120   |          
| Family                | $384         | $336         | $408   | $312   |

#### Kaiser 2020 Calendar Year Plans  

**Coverages:**

| Plan Details               | HMO 20 NorCal        | HMO 20 SoCal     | HMO 20 CO      | HMO 20 HI       |
|----------------------------|:--------------------:|:---------------:|:---------------:|:---------------:|
| Deductible (Single/Family) | $0 / $0              | $0 / $0         | $0 / $0         | $0 / $0         |
| OOP Max (Single/Family)    | $1,500 / $3,000      | $1,500 / $3,000 | $2,000 / $4,000 | $2,500 / $7,500 |
| PCP/Specialist Copay       | $20 / $35            | $20 / $35       | $20 / $35       | $15 / $15       |
| Emergency Room             | $50                  | $100            | $250            | $100            |
| Urgent Care                | $20                  |                 | $50             | $15             |
| Hospital Inpatient         | $250/admit           | $250/visit      | $300/admit      | 10%             |
| Hospital Outpatient        | $35/procedure        | $35/procedure   | $100/procedure  | 10%             |
| **Rx - Deductible**        |                      |                 |                 |                 |
| Generic                    | $10                  | $10             | $10             | $0              |
| Brand - Preferred          | $35                  | $35             | $30             | $0              |
| Brand - Non-Preferred      | $35                  | $35             | $50             | $0              |
| Specialty Drugs            | 20% up to $150       | $35             | 20% up to $150  | $0              |

**Team Member Costs:**

The following costs are monthly rates which would be deducted from your paycheck.

| Tier                     | HMO CA North | HMO CA South | HMO CO | HMO HI |
|--------------------------|:------------:|:------------:|:------:|:------:|
| Team Member Only         | $0           | $0           | $0     | $0     |
| Team Member + Spouse     | $228         | $228         | $264   | $168   |
| Team Member + Child(ren) | $180         | $180         | $216   | $132   |          
| Family                   | $348         | $348         | $432   | $324   |


### Infertility Services

Infertility services are provided through the UHC HSA plan. Please consult the legal SBC for more information or reach out to People Operations.

### Transgender Medical Services

Recently, the [United States Department of Health and Human Services](https://www.hhs.gov/) released a [mandate on transgender non-discrimination](https://www.hhs.gov/civil-rights/for-individuals/section-1557/nondiscrimination-health-programs-and-activities-proposed-rule/index.html). As part of this mandate, medical carriers were given time to review their current policies and update to reflect the mandate. Because there is a wide range of services, treatment, and goals for transgender patients, team members are encouraged to contact their carrier directly to have these discussions.

### Pregnancy & Maternity Care

With medical plans, GitLab offers pregnancy and maternity care. Depending on the plan you selected, your coverages may differ for in-network vs out-of-network, visits, and inpatient care. Please contact Lumity or People Operations with any questions about your plan.

## Dental

Dental is provided by Cigna, plan: DPPO.

Dental does not come with individualized insurance cards from Cigna, although you can download them by setting up a Cigna account through https://my.cigna.com. Lumity's site will house individualized ID cards team members can access at any time. For the most part, dental providers do not request or require ID cards as they look up insurance through your social security number. If you need additional information for a claim please let People Ops know. Cigna'a mailing address is PO Box 188037 Chattanooga, TN, 37422 and the direct phone number is 800-244-6224.

When submitting a claim, you can mail it to Cigna Dental PO Box 188037 Chattanooga, TN, 37422 or fax it to 859-550-2662. 

### Dental 2019 and 2020 Calendar Year Plan

Please note there are no changes to our dental plan from Calendar Year 2019 to Calendar Year 2020

**Coverages:**

| Plan Details                         | [DPPO](https://drive.google.com/file/d/1P0WLv1-5sylGHXaM8PSA13Uyz1gm5HvO/view?usp=sharing)       |  
|--------------------------------------|:----------:|
| Deductible                           | $50 / $150 |
| Maximum Benefit                      | $2,000     |
| Preventive Care CoInsurance (in/out) | 0% / 0%    |
| Basic Care Coinsurance (in/out)      | 20% / 20%  |
| Major Care Coinsurance (in/out)      | 50% / 50%  |
| Out of Network Reimbursement         | 90th R&C   |
| **Orthodontia**                      |            |
| Orthodontic Coinsurance (in/out)     | 50% / 50%  |
| Orthodontic Max Benefits             | $1,500     |

**Team Member Costs:**

The following costs are monthly rates which would be deducted from your paycheck.

| Tier                     | DPPO |
|--------------------------|:----:|
| Team Member Only         | $0   |
| Team Member + Spouse     | $12  |
| Team Member + Child(ren) | $18  |           
| Family                   | $36  |

## Vision

Vision is provided by Cigna.

When submitting a claim, you can mail it to Cigna Vision PO Box 385018 Birmingham, AL 35238 or fax it to 859-550-2662.

### Vision 2019 and 2020 Calendar Year Plan

Please note there are no changes to our dental plan from Calendar Year 2019 to Calendar Year 2020.

**Coverages:**

| Plan Details                      | [Vision](https://drive.google.com/file/d/1u7_P1GRRWCtSXBanGx02uqd5dbhkZemZ/view?usp=sharing)       |  
|-----------------------------------|:------------:|
| Frequency of Services             | 12 / 12 / 12 |
| Copay Exam                        | $20          |  
| Copay Materials                   | -            |
| Single Vision                     | $0           |  
| Bifocal                           | $0           |
| Trifocal                          | $0           |
| Frame Allowance                   | up to $130   |
| Elective Lenses Contact Allowance | up to $130   |

**Team Member Costs:**

The following costs are monthly rates which would be deducted from your paycheck.

| Tier                     | Vision |
|--------------------------|:------:|
| Team Member Only         | $0     |
| Team Member + Spouse     | $2.40  |
| Team Member + Child(ren) | $1.80  |           
| Family                   | $4.80  |

## Discovery Benefits

If you are enrolled in an HSA, FSA, or commuter benefits, the funds are held through Discovery Benefits. FSAs have a $500 rollover each calendar year.

You will only receive on debit card upon enrollment. To obtain a second card (for a dependent, etc.) you will need to login to your account on Discovery or call and they will send one to your home of record.

If you would like to transfer your HSA from a previous account, please contact Discovery Benefits and request a HSA Transfer funds form. On the form you will put your old HSA provider’s account number and any other required personal information. You will then submit the form to Discovery, and they will get in contact with your old HSA provider and process the transfer of funds. You can reach Discovery Benefits at 866.451.3399.

If you would like to adjust your HSA contributions please log into [Lumity](https://lumity.com/).

## Basic Life Insurance and AD&D

GitLab offers company paid [basic life](https://drive.google.com/file/d/1bSon6Esb6Y67bCwtv-owbrhm5wZdzvzw/view?usp=sharing) and [accidental death and dismemberment (AD&D)](https://drive.google.com/file/d/1Yp7oXD2kB8iBAbmelxxHZvFoAYoteWd0/view?usp=sharing) plans through Cigna (UHC in Calendar year 2020). The Company pays for basic life insurance coverage valued at two times annual base salary with a maximum benefit of $250,000, which includes an equal amount of AD&D coverage.

## Group Long-Term and Short-Term Disability Insurance

GitLab provides a policy through Cigna (UHC in Calendar year 2020) that may replace up to 66.7% of your base salary, for qualifying disabilities. For [short-term disability](https://drive.google.com/file/d/1dn7BkwrOmAN1bEKfoz_dW6Eu7VdRwTwk/view?usp=sharing) there is a weekly maximum benefit of $2,500; for [long-term disability](https://drive.google.com/file/d/1cBDAGjn0rZHFZcVxYQ5mz6pcu-JNOR_I/view?usp=sharing) there is a monthly benefit maximum of $12,500.

**Process for Disability Claim**

1. If an team member will be unable to work due to disability for less than 25 calendar days, no action is needed and the absence will be categorized under [paid time off](/handbook/paid-time-off/).
1. Since the short-term disability insurance has a 7-day waiting period, the team member should decide on day 18 whether they will be able to return to work after 25 calendar days. If they will not be able to return, they should inform the Compensation & Benefits team of their intent to go on short-term disability and apply for short-term disability at this time by sending the Compensation & Benefits team a completed [Leave Request Form](https://drive.google.com/file/d/0B4eFM43gu7VPazNlQ3ktMmVVQVl6UmowZFRlQXdtaVExOW8w/view). While the team member is on short-term disability (which covers 66.7%), GitLab will supplement the remaining 33.3% through payroll if the team member has been employed for more than a year. Benefit coverage will also continue for the time the team member is on short-term disability.
1. At the end of the maximum benefit period for short-term disability of 12 weeks, the team member will determine whether they are able to return back to work. If they are unable to, the team member will be moved to unpaid leave and will have the option to continue their benefits by electing [COBRA coverage](https://www.dol.gov/sites/dolgov/files/ebsa/about-ebsa/our-activities/resource-center/faqs/cobra-continuation-health-coverage-consumer.pdf). The team member will be eligible to apply for long-term disability at this time.

## Life Assistance Program

GitLab team members in the United States are eligible for a complementary life assistance program as a result of enrollment in the Life Insurance plan through Cigna. More information can be found on the [Cigna brochure](https://drive.google.com/file/d/1a6atOvNzbYC2K5BYXn8skiJw6RgJMWnJ/view?usp=sharing) for the following benefits: behavioral counselor benefits, monthly webinars, legal consultations, financial consultations.

## 401k Plan

The company offers a 401k plan in which you may make voluntary pre-tax contributions toward your retirement.

### Administrative Details of 401k Plan

1. You are eligible to participate in GitLab’s 401k as of your hire date. There is no auto-enrollment. You must actively elect your deductions.
1. You will receive an invitation from [Betterment](https://www.betterment.com) who is GitLab's plan fiduciary. For more information about Betterment please check out this [YouTube Video](https://www.youtube.com/watch?v=A-9II-zBq1k).
1. Any changes to your plan information will be effective on the next available payroll.
1. Once inside the platform you may elect your annual/pay-period contributions and investments.
1. Please review the [Summary Plan Document](https://drive.google.com/file/d/1k1xWqZ-2HjOWLv_oFCcyBkTjGnuv1l_B/view?usp=sharing) and [QDIA & Fee Disclosure](https://drive.google.com/file/d/1gWxCI4XI01gofUu9yqKd4QyXeM27Xv57/view?usp=sharing). If you have any questions about the plan or the documents, please reach out to People Ops.
1. ADP payroll system monitors and will stop the 401(k) contribution when you have reached the IRS limit for the year.

### 401(k) Match

GitLab offers matching 50% of contributions on the first 6% with a yearly cap of 1,500 USD. All employer contributions are pre-tax contributions. Team members can still make Roth team member contributions and receive pre-tax employer contributions.

**Vesting:**

Employer contributions vest according to the following schedule:

| Years of Vesting Service             | Vesting Percentage |
|--------------------------------------|:------------------:|
| Less than One Year                   | 0%                 |
| One Year but less than Two Years     | 25%                |
| Two Years but less than Three Years  | 50%                |
| Three Years but less than Four Years | 75%                |
| Four or More Years                   | 100%               |

*Employee* contributions are the assets of those team members and are not applicable to this vesting schedule.

**Administration of the 401(k) Match:**
* The employer will use the calculation on each check date effective as of January 1, 2019.
* The team member must have a contribution for a check date to be eligible for the employer match.
* Employer matching will be released into participant accounts three business days after the check date.
* For team members who defer more than 6% on each check date, Payroll and People Operations will conduct a true up quarterly.

## Optional Plans Available at Team Member Expense

### Flexible Spending Account (FSA) Plans

FSAs help you pay for eligible out-of-pocket health care and dependent day care expenses on a pretax basis. You determine your projected expenses for the Plan Year and then elect to set aside a portion of each paycheck into your FSA.

### Supplemental Life/Accidental Death and Dismemberment Insurance

If you want extra protection for yourself and your eligible dependents, you have the option to elect [supplemental life insurance](https://drive.google.com/file/d/1kvdeLWBz6Tvz5S6XXB9jq3wNKUuzNkYz/view?usp=sharing):
   * $10,000 Increments up to the lesser of 6x annual salary or $750,000 for team members
   * $5,000 Increments up to the lesser of $250,000 or 100% of team member election for spouses and domestic partners
   * $10,000 of coverage available for children

Certain coverage increases must be approved by the insurance carrier.

In Calendar year 2020, these plans will move from Cigna to UHC.

### Commuter Benefits

GitLab offers [commuter benefits](https://drive.google.com/file/d/0B4eFM43gu7VPek1Ia0ZqYjhuT25zYjdYTUpiS1NFSXFXc0Vn/view?usp=sharing) which are administered through Discovery Benefits. The contribution limits from the IRS for 2019 are $265/month for parking and $265/month for transit. These contributions rollover month to month.

## Team Member Discount Platforms

US team members have access to two discount platforms offered through ADP and Lumity. These platforms provide discounts for national and local brands and services.

To access LifeMart through ADP:
1. Login to ADP using the following link: [(https://workforcenow.adp.com.)]
1. Click on the "MYSELF" tab in the navigation bar, hover your mouse over "Benefits" in the dropdown menu, and click "Employee Discounts - Life Mart".
1. Confirm the email you use to access ADP and click "View my discounts" to enter the website.

To access PerkSpot through Lumity:
1. Navigate to https://lumity.perkspot.com/.
1. Click "Create an Account" and fill out the form in order to register.

## Monthly Health Bill Payments

The People Operations Analyst will review and initiate payment for all monthly health bills in the United States.

* All bills are available on the first of the month and should be paid by the 13th.
* Lumity will send a reconciliation report breaking down the bills by department. People Ops will transfer the department breakdown and Group Invoice to the "Lumity Bill Reconciliations" google sheet.
* TODO Build in audit procedure to verify bills against current elections to ensure accuracy.
* People Operations will login to each admin platform and pay the bills using the banking information and links found in the 1password note: "Monthly Heath Bills"
* People Operations will then email a detailed breakdown of the amount/department totals to `ap@gitlab.com` for accounting purposes.

## GitLab Inc. United States Leave Policy:

Based on the Family and Medical Leave Act, or [FMLA](https://www.dol.gov/whd/fmla/), US team members are "eligible to take job-protected leave for specified family and medical reasons with continuation of group health insurance coverage under the same terms and conditions as if the team member had not taken leave." For more information on what defines an eligible team member and medical reason please visit the [Electronic Code of Federal Regulation](http://www.ecfr.gov/cgi-bin/text-idx?c=ecfr&sid=d178a2522c85f1f401ed3f3740984fed&rgn=div5&view=text&node=29:3.1.1.3.54&idno=29#sp29.3.825.b) for the most up to date data.

### Apply For Parental Leave in the US

1. Notify People Operations of intention to take parental leave at least 30 days in advance, or as soon as reasonable.
    - For a birthing parent (maternity leave), the team member will fill out the [Leave Request Form](https://drive.google.com/file/d/0B4eFM43gu7VPazNlQ3ktMmVVQVl6UmowZFRlQXdtaVExOW8w/view?usp=sharing) and email the form to People Ops.
      - Add your name, date of birth, address, social, and date of birth to the "To Be Completed By Employer" section. People Ops will fill out the rest of the form upon submission.
      - Team member will need to populate the "To Be Completed By The Claimant" and "To Be Completed By Attending Physician"
      - Review and sign the Disclosure Authorization. Also, review the Important Claim Notice.
      - People Ops will finish populating the "To Be Completed By the Employer/Administrator" and "Employers'Administrator's Certification" section of the form.
      - Once the form is complete, People Ops will email the form to Cigna using the email address in 1password.
      - Cigna will send a confirmation or denial of the claim via regular mail. Also, a Claims Manager will reach out to the team member by phone.
      - Once the team member has filed a claim for STD, they will need to confirm the start and end dates of the STD.
      - The typical pay for STD is six weeks for a vaginal birth and eight weeks for a c-section. STD can be extended if the team member has a pregnancy related complication (bed rest, complications from delivery, etc).
      - TODO Outline letters to send and process to apply for STD.
    - For non-birthing parents (including maternity leave when adopting and paternity leave), email People Operations with details of your planned leave.
1. People Operations will confirm [payroll details](#payroll-processing-during-parental-leave) with the Controller.  
1. The team member will notify People Operations on their first day back to work.
1. TODO Outline process to return the team member to work

### Payroll Processing During Parental Leave

**Paternity Leave**
Paternity Leave is not covered under Short Term Disability, so if the team member is eligible for 100% of pay, payroll would enter 100% of the pay period hours under "Leave with Pay."   

**Maternity Leave**
For maternity leave, GitLab will verify 100% of the wages are paid for eligible team members through payroll and Short-term Disability (STD).

1. While the team member is on short-term disability (which covers 66.7%), GitLab will supplement the remaining 33.3% through payroll.
  * The team member will inform People Operations when their short term disability is set to begin and end.
  * There is a seven day elimination period for short term disability that is not paid through Cigna, where GitLab will need to supplement the entire wage through payroll.
    * For instance, if Cigna has approved STD for January 1 - February 12, January 1 - January 7 would not be paid through STD.
    * If any adjustments need to be made to a payroll that has already passed, People Ops will coordinate with the Payroll Lead to ensure retroactive payments are made.
  * For the days the team member is paid through STD, payroll will adjust leave with pay hours to equal 33.3% and leave without pay hours to equal 66.7%.
    * For example, if there are 80 hours in the pay period you would input 26.66 hours Leave with Pay / 53.34 Leave without Pay.
1. When short-term disability ends, payroll will need to have 100% of the hours fall under leave with pay.

Note: Also, ensure the disability is not is not capped out at the current maximum or that will also need to be supplemented in each pay cycle.
