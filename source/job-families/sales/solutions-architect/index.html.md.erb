---
layout: job_family_page
title: "Solutions Architect"
---

Solutions Architects are the trusted advisors to GitLab prospects and clients, showing how the GitLab solutions address client business requirements. Solutions Architects are responsible for actively driving and managing the technology evaluation and validation stages of the sales process. Solutions Architects are the product advocates for GitLab’s Enterprise Edition, serving as a trusted advisor to the client, focusing on the technical solution while also understanding the business outcomes the customer is trying to achieve.

The Solutions Architect helps drive value and change in the world of software development for one of the fastest growing platforms. By applying solution selling and architecture experience from planning to monitoring, the Solutions Architect supports and enables successful adoption of the GitLab platform. Solutions Architects work directly with GitLab's top enterprise customers. Solutions Architects work collaboratively with Sales, Engineering, Product Management and Marketing organizations.

This role provides technical guidance and support through the entire sales cycle. Candidates will have the opportunity to help shape and execute a strategy to build mindshare and broad use of the GitLab platform with enterprise customers by becoming the trusted advisor. The ideal candidate must be self-motivated with a proven track record in software/technology sales or consulting. The ability to connect technology with measurable business value is critical to a Solutions Architect. Candidates should also have a demonstrated ability to think strategically about business, products, and technical challenges.

To learn more, see the [Solutions Architect handbook](/handbook/customer-success/solutions-architects)

### Responsibilities

* Primarily engaged in a technical consultancy role, providing technical assistance and guidance specific to enterprise level implementations, during the pre-sales process by identifying customer's technical and business capabilities in order to design an appropriate GitLab solution
* In partnership with the sales team, formulate and execute a sales strategy to exceed revenue objectives through the adoption of GitLab
* Educate customers of all sizes on the value proposition of GitLab, and participate in all levels of discussions throughout the organization to ensure our solution is set up for successful deployment
* Work on­site with strategic, enterprise­ class customers, delivering solutions architecture consulting, technical guidance, knowledge transfer and establish “trusted advisor status”
* Guide technical evaluations via POC/POV ownership, RFP/audit support and workshop design
* Capture and share best-practice knowledge amongst the GitLab community
* Author or otherwise contribute to GitLab customer-facing publications such as whitepapers, blogs, diagrams or the GitLab Handbook
* Build deep relationships with senior technical individuals within customer environments to enable them to be GitLab advocates
* Serve as the customer advocate to other GitLab teams including Product Development, Sales and Marketing
* Present GitLab platform strategy, concepts and roadmap to technical leaders within customer organizations

### Requirements

* Presentation skills with a high degree of comfort speaking with executives, IT Management, and developers
* Strong written communication skills
* High level of comfort communicating effectively across internal and external organizations
* Ideal candidates will preferably have 7 plus years software industry or software technical sales experience with a proven track record of solution sales
* Significant experience with executive presence and a proven ability to lead and facilitate executive meetings and workshops
* Deep knowledge of the end-to-end software development lifecycle and development pipeline
* Understanding of continuous integration, continuous deployment, chatOps and cloud native
* Experience with waterfall, Agile (SCRUM, Kanban, etc) and able to discuss workflows for different software development processes
* Experience with modern development practices strongly preferred:
  * Kubernetes
  * Docker
  * Linux
  * Package Management
  * DevOps Pipelines (CI/CD)
  * Application Security (SAST, DAST)
  * Cloud (AWS, GCP, Azure)
  * Application Performance Monitoring
  * System Logging
* Experience with several of the following tools strongly preferred:
  * Ruby on Rails, Java, PHP, Python
  * Git, Bitbucket, GitHub, SVN
  * Jira, VersionOne, TFS
  * Jenkins, Travis, CircleCI
  * Veracode, Fortify
  * Artifactory, Nexus
  * New Relic, Nagios
* Understand mono-repo and distributed-repo approaches
* Understand BASH / Shell scripting
* Ability to travel up to 35%
* B.Sc. in Computer Science or equivalent experience
* Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks)
* Ability to use GitLab

<%= partial("job-families/sales/performance", :locals => { :roleName => "Solutions Architect" }) %>

---
## Levels

### Junior Solutions Architect

Junior Solutions Architects share the same requirements and responsibilities outlined above, but typically join with less relevant experience, alternate background and/or less industry exposure than an intermediate enterprise Solutions Architect. The Junior Solutions Architect role most commonly interacts with small to mid-size companies rather than large enterprises.

### Senior Solutions Architect

The Senior Solutions Architect role extends the Solutions Architect role to include more responsibilities based on experience, expertise and team influence.

#### Additional Responsibilities

  * Solve technical customer issues of large scope and high complexity
  * Exert influence on the overall objectives and long-range goals of the team
  * Provide mentorship for Junior and Intermediate Solutions Architects on the team to help them grow in their technical knowledge
  * Work cross-departmentally to find solutions to complex scenarios and integration issues
  * Demonstrate leadership in new feature and technology adoption and teaching
  * Help drive team expertise and technical thought leadership
  * Propose improvements and innnovation for customer calls and demos based on market direction
  * Maintain deep knowledge of the entire GitLab application
  * Represent GitLab in thought leadership as a speaker at field events or as an author in GitLab-focused publications and blogs

A Senior Solutions Architect may choose to pursue the management track after this.

---
## Specialties

Read more about what a [specialty](/handbook/hiring/#definitions) is at GitLab.

### Federal Solutions Architect

#### Additional Responsibilities

* Participating in conferences and trade shows and interacting with government customers in attendance
* Contribute to the creation of case studies, white papers, and media articles for government customers and/or partner
* Consistently provide world-class customer service during pre-sales, implementation, and post-sales activities
* Manage all technical aspects of the sales cycle: creating high quality professional presentations, custom demos, proof of concepts, deliver technical deep dive sessions & workshops, differentiate GitLab from competition, answering RFI, RFPs, etc.
* TS/SCI Security Clearance
* Must be located in the Washington DC metro area
* Knowledge and at least 4 years of experience with Federal customers
* Ability to travel up to 50%
* Understand mono-repo and distributed-repo approaches

### Commercial Solutions Architect

#### Responsibilities

* Strong oral and written communication skills
* High level of comfort communicating effectively across internal and external organizations
* Ideal candidates will preferably have 3 plus years software industry or software technical sales experience
* Some experience with executive presence and an ability to lead and facilitate executive meetings and workshops
* Comprehensive knowledge of software development lifecycle and development pipeline
* Understanding of continuous integration, continuous deployment, chatOps and cloud native
* Experience with waterfall, Agile (SCRUM, Kanban, etc) and able to discuss workflows for different software development processes
* Hands-on experience with modern software development strongly preferred
* Experience with current software development tools strongly preferred
* Understand mono-repo and distributed-repo approaches
* Understand BASH / Shell scripting
* Ability to travel up to 20%
* B.Sc. in Computer Science or equivalent experience
* Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks)

### Manager, Solutions Architects

GitLab is a hyper growth company searching for people who are intelligent, aggressive and agile with strong skills in technology, sales, business, communication and leadership. Desire to lead through change is a must.

The Manager, Solutions Architects role is a management position on the front lines. This is a player/coach role where the individual is expected to be experienced in and have advanced insight to the GitLab platform. The individual will contribute to territory and account strategy as well as driving the execution directly and indirectly. The individual will need to be very comfortable giving and receiving positive and constructive feedback, as well as adapting to environmental change and retrospecting on successes and failures. The Manager, Solutions Architects will work together with the other managers within the customer success organization to help execute on strategies and vision with the Director.

#### Responsibilities

- Oversee the day-to-day operations of your team of Solutions Architects
- Work as an individual contributor for key accounts as needed or requested
- Participate in technical sales calls, and contribute to creating sales and evaluation strategy at the account level
- Provide technical leadership and platform strategy to the team as well as the larger sales organization
- All aspects of people management, including hiring, development, training, regular 1:1s and annual reviews
- Hire, train and retain top talent
- Provide feedback and direction, both positive and constructive, to personnel in a timely manner
- Build a strong team and provide satisfaction among your team and customers
- Influence both internal and external C-level executives through presentations and whiteboard sessions
- Work collaboratively with other groups, including Sales, Professional Services, Support, Engineering, Technical Account - Managers and Product Management, to ensure effective operation of your team, achieve the technical win, and ensure ongoing customer satisfaction
- Formulate best practices for presentations, demos, and POCs as well as overall sales strategy
- Act as a trusted advisor to higher level management on strategic opportunity reviews, emerging competitive threats, product direction, and establishing sales objectives and strategies
- Work with the Customer Success Director to help establish and manage Solutions Architect goals and responsibilities
- Assist in development of thought leadership, event-specific and customer-facing presentations
- Share hands-on technical preparation and presentation work for key accounts
- Take the lead in hiring and onboarding new SA’s
- Ensure the SA team exceeds corporate expectations in core knowledge, communication and execution
- Define and maintain a high bar for team member expectations and enable the team to achieve it
- Challenge the team and yourself to continually learn and grow as trusted advisors to clients
- Evangelize new product features & provide customer feedback to GitLab product management and engineering groups
- Ensuring the Solutions Architects engage the Technical Account Manager during the pre-sales so that  the post-sales technical process handoff for seamless Drive customer adoption and success by providing oversight, adoption - recommendations, opportunities for greater service
- Ensure timely resolution of POC/POV issues by coordinating support responses
- Maintain a deep understanding of each customer’s business as well as their technical environments -- become a trusted advisor to our customers
- Support the Sales team with opportunity qualification, demonstrations, Proof of Concept presentations (POC), RFP responses, and business justification in a pre-sales capacity
- Mentor and develop the existing Solutions Architect team
- Be able to present complex technical solutions to clients
- Present credibly to engineering decision makers and technology executives
- Develop and execute solution selling strategies and identify opportunities for process improvements
- Remain knowledgeable and up-to-date on GitLab releases

#### Requirements

All requirements for the Solutions Architect role apply to the Manager, Solutions Architects position, as the management position will need to understand and participate with the day-to-day aspects of the Solutions Architect role in addition to managing the team. Additional requirements include:

- Experienced in and with advanced insight to the GitLab platform
- Experienced in giving and received positive and constructive feedback
- Able to adapt to environmental change and restrospecting on success and failures
- Previous leadership experience is a plus
- Experienced in collaborating with other managers and executing strategies

### Director, Solutions Architects

GitLab’s Director, Solution Architects provides strategic vision and builds and develops an exceptional team of Solution Architects. This person manages the team responsible for leading technical evaluation and validation of prospects’ and customers’ needs to drive new customer acquisition and expansion for existing customers. Partnering closely with Sales and Customer Success teams, this leader will create strategies and operations to effectively and efficiently accelerate GitLab’s growth and improve prospects’ and customers’ experiences.

#### Responsibilities

- Hire, mentor and develop exceptional team of Solutions Architects
- Develop strategies and operations to improve win rates by aligning and articulating GitLab's solution to deliver on specific customer requirements and desired business outcome
- Identify and lead initiatives and programs to establish and scale the organization and operations for future growth
- Develop processes and metrics and KPIs to improve effectiveness and efficiency of technical evaluations, demos and proof-of-concept engagements
- Partner with Sales, Channels and Alliances teams to align on overall strategy and priorities and provide support for specific prospects, customers and partners
- Develop and foster relationships for key customers at the technical champion and executive level
- Partner with sales leadership to align with and deliver to regional and account plans, strategies and quarterly goals
- Collaborate with Sales and Customer Success to improve engagement models and ensure the appropriate coverage of prospects and customers
- Partner with Product, Engineering, Marketing and Services teams to provide feedback to improve products, services and value messaging based on field experiences and feedback
- Partner with Sales Operations to ensure efficient and ongoing enablement and development of the team
- Be a role model for GitLab’s values and culture

#### Requirements

- 5+ years experience leading technical pre-sales teams (i.e., Solutions Architect and/or Sales Engineering teams)
- 2+  years experience building and leading teams of managers
- Demonstrated ability to build and improve strategies and operations to improve technical assessment processes and team enablement
- Experience with software development lifecycle processes and tools as well as agile and/or DevOps practices
- Knowledgeable with cloud technologies (e.g., Kubernetes, Docker), application security (SAST, DAST) and/or cloud deployment models (AWS, GCP, Azure)
- Experience selling technical solutions to technical staff, management and executive stakeholders
- Proven experience partnering with the broader organization (sales, channel, alliances, product and engineering, marketing and customer success)
- B.S. in Computer Science, Engineering or equivalent experience

#### Specialties

- Enterprise Solutions Architect Director will oversee teams of SA's, focused on GitLab’s Enterprise customers
Requirement
- 7+ years of enterprise sales experience and well-versed in enterprise engagement approaches

- Commercial Solutions Architect Director will oversee teams of SA's, focused on GitLab’s Commercial customers (i.e., MidMarket and SMB)
Requirement
- 7+ years sales experience and well-versed in scale engagement approaches for midmarket and SMB

---
## Hiring Process

Candidates can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

### Individual Contributor Hiring Process:

- Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
- Next, candidates will be invited to schedule a first interview with the SA team Manager
- Candidates may be invited to schedule an interview with a Solutions Architect peer or other SA team Manager
- Then, candidates will be required to deliver a demo of GitLab to a panel of Customer Success attendees using the [Demo Guide](https://docs.google.com/document/d/12Dw4p25R5FaLnLpwFGtEr0kxxoWklWJL9FyP-NTITKY/edit?ts=5c48c337)
- Candidates may be invited to additional interviews
- Successful candidates will be made an offer after references are verified

### Management Hiring Process:

- Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
- Next, candidates will be invited to schedule a first interview with a Customer Success VP or Director
- Candidate will then schedule an interview with an SA management peer
- Then, candidates will present a business plan to include 30/60/90 day approach, outcomes, and metrics
- Candidates may be invited to additional interviews
- Successful candidates will be made an offer after references are verified

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing/).

---
## Compensation

You will typically be paid 75% as base, and 25% based on meeting the global sales goal of incremental ACV. Also see the [Sales Compensation Plan](/handbook/finance/sales-comp-plan/).

---
