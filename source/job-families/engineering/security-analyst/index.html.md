---
layout: job_family_page
title: "Security Analyst"
---

As a member of the security team at GitLab, you will be working towards raising the bar on security. We will achieve that by working and collaborating with cross-functional teams to provide guidance on security best practices.

The [Security Team](/handbook/engineering/security) is responsible for leading and
implementing the various initiatives that relate to improving GitLab's security.

## Responsibilities

- Develop security training and guidance to internal development teams
- Assist with recruiting activities and administrative work
- Communication
  * Handle communications with independent vulnerability researchers.
  * Educate other developers on workflows and processes.
  * Ability to professionally handle communications with outside researchers, users, and customers.
  * Ability to communicate clearly on related security issues.

## Requirements

- You have a passion for security and open source
- You are a team player, and enjoy collaborating with cross-functional teams
- You are a great communicator
- You employ a flexible and constructive approach when solving problems
- You share our [values](/handbook/values), and work in accordance with those values
- Ability to use GitLab

### Senior Analyst

* Leverages security expertise in at least one specialty area
* Triages and handles/escalates security issues independently
* Conduct reviews and makes recommendations
* Great written and verbal communication skills
* Screen security candidates during hiring process

### Anti-Abuse Analyst

The Anti-Abuse Analyst is responsible for leading and implementing the various initiatives that relate to improving GitLab's security including:

- Handle tickets/requests escalated to abuse
- Handle DMCA, phishing, malware, botnet, intrusion attempts, DoS, port scanning, spam, spam website, PII and web-crawling abuse reports to point of mitigation of abuse
- Verify proper classification of incoming abuse reports
- Execute messaging to customers on best practices
- Monitoring email, forums, and other communication channels for abuse, and responding accordingly
- Assist with recruiting activities and administrative work
- Making sure internal knowledge reference pages are updated
- Handle communications with independent vulnerability researchers and triage reported abuse cases.
- Educate other developers on anti-abuse cases, workflows and processes.
- Ability to professionally handle communications with outside researchers, users, and customers.
- Ability to communicate clearly on anti-abuse issues.

### Compliance Analyst
 
Compliance Analysts focus on defining and shaping GitLab’s compliance programs, help build compliance features into our product, and are experts in all things compliance. We are looking for people who are comfortable building transparent compliance programs and who have worked with and understand how compliance works with cloud-native technology stacks.

Responsibilities
- Develop roadmap based on customer needs e.g.: GDPR, SOC 2, FIPS 140-2
- Align other security specialist activities with the compliance roadmap
- Conduct security reviews and makes recommendations
- Screen security candidates during hiring process
- Coordinate work of internal and external auditors or advisors as needed
- Handle communications with customers and answer security compliance questionnaires
- Educate others on compliance workflows and processes
- Professionally handle communications with internal and external stakeholders
- Communicate clearly on compliance issues

Requirements
- Proven experience defining and shaping compliance programs
- Demonstrated experience with at least two security control frameworks (e.g. SOC2, ISO, NIST, PCI, etc.)
- Proven experience building compliance features into products
- Experience with, or a passion for transparent compliance programs
- Working understanding of how compliance works with cloud-native technology stacks
- Passion for security and open source
- Team player, and enjoy collaborating with cross-functional teams
- Great communication skills
- Proven ability to employ a flexible and constructive approach when solving problems
- You share our values, and work in accordance with those values


### External Communications Analyst

The External Communications Team leads customer advocacy, engagement and communications in support of GitLab Security Team programs. Initiatives for this specialty include:

- Increase engagement with the hacker community, including our public bug bounty program.
- Build and manage a Security blogging program.
- Develop social media content and campaigns, in collaboration with GitLab social media manager.
- Manage security alert email notifications.
- Collaborate with corporate marketing, PR, Community Advocates and Technical Evangelism teams to help identify opportunities for the Security Team to increase industry recognition and thought leadership position.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Qualified candidates receive a short questionnaire from our Recruiting team
- Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with our Recruiting team
- Next, candidates will be invited to schedule an interview with Security Engineer
- Candidates will then be invited to schedule an interview with Director of Security
- Candidates will then be invited to schedule an additional interview with VP of Engineering
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

As always, the interviews and screening call will be conducted via a video call.
See more details about our hiring process on the [hiring handbook](/handbook/hiring).

## Performance Indicators

Security Analysts have the following job-family performance indicators.

### External Communications
* [HackerOne Outreach and Engagement](/handbook/engineering/security/performance-indicators/#hackerone-outreach-and-engagement)
